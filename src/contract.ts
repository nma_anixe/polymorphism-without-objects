export type Contract = {
  id: string;
  hotel: { name: string };
  value: number;
  type: "sale" | "purchase";
};

export type DisplayableContract = Contract & { displayValue: string };
