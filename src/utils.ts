// The main benefit of polymorphism is that
// it makes the code easily extensible!

//Method declarations are
// decoupled from the multimethod initialization!

// A dispatch function could emit any value.
// It gives us more flexibility than with OOP polymorphism!
import Hash from 'object-hash';

type CustomFn<T> = (obj: T, ...args: any[]) => any;

export function multi2<T>(
  dispatcher: CustomFn<T>,
  map: { [key: string]: CustomFn<T> } = {},
  defaultCase?: CustomFn<T>
) {
  function multiFunction(obj: T, ...args: any[]) {
    const key = dispatcher(obj, ...args);
    const hashedKey = Hash.sha1(key);
    if (map[hashedKey] == null) {
      if (defaultCase == null) {
        throw new Error("Method not found for " + key);
      }
      return defaultCase(obj, ...args);
    }
    return map[hashedKey](obj, ...args);
  }
  // transfer props
  multiFunction._dispatcher = dispatcher;
  multiFunction._map = map;
  multiFunction._defaultCase = defaultCase;

  return multiFunction;
}

// Ugly type (Don't know how to infer O from ReturnType<typeof Multi2>)
type MultiType<T> = CustomFn<T> & {
  _dispatcher: CustomFn<T>;
  _map: {
    [key: string]: CustomFn<T>;
  };
  _defaultCase: CustomFn<T> | undefined;
};

export function method2<T>(fn: CustomFn<T>, key?: any) {
  const hashedKey = key ? Hash.sha1(key) : null;
  return (koko: MultiType<T>) => {
    return multi2(
      koko._dispatcher,
      { ...koko._map, ...(hashedKey != null && { [hashedKey]: fn }) },
      hashedKey == null ? fn : koko._defaultCase
    );
  };
}

export class Multi<T> {
  constructor(
    private dispatcher: CustomFn<T>,
    private map: { [key: string]: CustomFn<T> } = {},
    private defaultCase?: CustomFn<T>
  ) {}

  add(fn: CustomFn<T>, key?: any): Multi<T> {
    // MUTABLE VERSION!
    if (key == null) {
      this.defaultCase = fn;
    } else {
      const hashedKey = Hash.sha1(key);
      this.map[hashedKey] = fn;
    }

    // IMMUTABLE VERSION!
    // const hashedKey = key ? Hash.sha1(key) : null;
    // return new Multi(
    //   this.dispatcher,
    //   {
    //     ...this.map,
    //     ...(hashedKey != null && { [hashedKey]: fn }),
    //   },
    //   hashedKey == null ? fn : undefined
    // );
    return this;
  }

  run(obj: T, ...args: any[]): any {
    const key = this.dispatcher(obj, ...args);
    const hashedKey = Hash.sha1(key);
    if (this.map[hashedKey] == null) {
      if (this.defaultCase == null) {
        throw new Error("Method not found for " + key);
      }
      return this.defaultCase(obj);
    }
    return this.map[hashedKey](obj, ...args);
  }
}
