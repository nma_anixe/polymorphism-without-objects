import { Contract, DisplayableContract } from './contract';
import { method2, Multi, multi2 } from './utils';

// Polymorphic Solutions
type Config = { shortenHotelNames: boolean };

export const shortenName = (name: string) => name.substr(0, 4) + "...";

const formatPurchaseContractEUR = (
  contract: Contract
): DisplayableContract => ({
  ...contract,
  hotel: { name: shortenName(contract.hotel.name) },
  displayValue: `Purchase ${contract.value} €`,
});

const formatSalesContractGB = (contract: Contract): DisplayableContract => ({
  ...contract,
  displayValue: `Sale ${contract.value} £`,
});

const formatContractDefaultUS = (contract: Contract): DisplayableContract => ({
  ...contract,
  displayValue: `${contract.value} $`,
});

function dispatcher2(contract: Contract, conf: Config) {
  return [contract.type, !!conf?.shortenHotelNames];
}

export const getViewContracts = new Multi(dispatcher2)
  .add(formatPurchaseContractEUR, ["purchase", true])
  .add(formatSalesContractGB, ["sale", false])
  .add(formatContractDefaultUS);

let _getViewContracts2 = multi2(dispatcher2);
_getViewContracts2 = method2(formatContractDefaultUS)(_getViewContracts2);
_getViewContracts2 = method2(formatPurchaseContractEUR, ["purchase", true])(
  _getViewContracts2
);
_getViewContracts2 = method2(formatSalesContractGB, ["sale", false])(
  _getViewContracts2
);
export const getViewContracts2 = _getViewContracts2;
