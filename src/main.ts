import { Contract, DisplayableContract } from './contract';
import { getViewContracts, getViewContracts2, shortenName } from './poly';
import './style.css';
import { method2 } from './utils';

const app = document.querySelector<HTMLDivElement>("#app")!;

const data: Contract[] = [
  { id: "1", value: 3.2, type: "purchase", hotel: { name: "Zante Hotel" } },
  { id: "2", value: 7.3, type: "sale", hotel: { name: "Oxford Hotel" } },
  { id: "3", value: 8, type: "purchase", hotel: { name: "Wasington Hotel" } },
];

// Solution 1
const sol1 = [
  getViewContracts.run(data[0], { shortenHotelNames: true }),
  getViewContracts.run(data[1], { shortenHotelNames: false }),
  getViewContracts.run(data[2]),
];

// Extensibility (Solution 1)
function formatSalesSeasonal(contract: Contract): DisplayableContract {
  return {
    ...contract,
    hotel: { name: shortenName(contract.hotel.name) },
    displayValue: `Seasonal contract: 50% discount!`,
  };
}
getViewContracts.add(formatSalesSeasonal, ["sale", true]);
// Let's format an already existing contract with different configuration
const result1 = getViewContracts.run(data[1], { shortenHotelNames: true });

// Solution 2
const sol2 = [
  getViewContracts2(data[0], { shortenHotelNames: true }),
  getViewContracts2(data[1], { shortenHotelNames: false }),
  getViewContracts2(data[2]),
];

// Extensibility (Solution 2)
const getViewContracts2Extended = method2(formatSalesSeasonal, ["sale", true])(
  getViewContracts2
);
// Let's format an already existing contract with different configuration
const result2 = getViewContracts2Extended(data[1], { shortenHotelNames: true });

const toElem = (x: string) => "<pre>" + x + "</pre>";
const stringify = (obj: any) => toElem(JSON.stringify(obj, null, 2));

app.innerHTML = `
  <h1>Polymorphism with MultiFunctions</h1>
  
  <h2>Contracts (Solution 1)</h2>
  <section>
    ${sol1.map(stringify).join("")}
    <h3>Extensibility (Solution 1)</h3>
    ${stringify(result1)}
  </section>

  <h2>Contracts (Solution 2)</h2>
  <section>
    ${sol2.map(stringify).join("")}
    <h3>Extensibility (Solution 2)</h3>
    ${stringify(result2)}
  </section>
`;
