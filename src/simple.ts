import { Animal } from './animal';

export function greet(animal: Animal): string | undefined {
  switch (animal.type) {
    case "dog":
      return `Woof Woof! My name is: ${animal.name}`;
    case "cat":
      return `Meow! I am: ${animal.name}`;
    case "cow":
      return `Moo! Call me ${animal.name}`;
    default:
      return `Hi! I am ${animal.name}`;
  }
}
